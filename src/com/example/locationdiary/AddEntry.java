package com.example.locationdiary;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class AddEntry extends Activity implements OnClickListener {
	
	private Button getLocationBtn;
	private ImageView saveEntry;
	private GPSTracker gps;
	private TextView locationText;
	private TextView dateText;
	private TextView timeText;
    private ImageView photo;
    private EntryDataSource dataSource;
    private EditText title;
    private EditText description;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_entry);
		
		getLocationBtn = (Button) findViewById(R.id.btnGetLocation);
		saveEntry = (ImageView) findViewById(R.id.btnSaveEntry);
		saveEntry.setOnClickListener(this);
		getLocationBtn.setOnClickListener(this);
		locationText = (TextView) findViewById(R.id.textViewLocation);
		gps = new GPSTracker(AddEntry.this);
		dateText = (TextView) findViewById(R.id.textViewDate);
		timeText = (TextView) findViewById(R.id.textViewTime);		
	    dateText.setText("Not set");
	    timeText.setText("Not set");
	    title = (EditText) findViewById(R.id.editTextTitle);
	    description = (EditText) findViewById(R.id.editTextDescription);
	    photo = (ImageView) findViewById(R.id.photoPic);
	    
	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_entry, menu);
		return true;
	}


    public void setTime(View v) {
        DialogFragment timeFragment = new TimeSelectionFragment();
        
        timeFragment.show(getFragmentManager(), "timeFragment");
    }

    public void setDate(View v) {
    	DialogFragment dateFragment = new DateSelectionFragment();
    	dateFragment.show(getFragmentManager(), "dateFragment");
    }
    
    public void setPicture(View v) {
    	Intent pickPhoto = new Intent(Intent.ACTION_PICK,
    	           android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
    	startActivityForResult(pickPhoto , 1);	
    }
        
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) { 
    	super.onActivityResult(requestCode, resultCode, imageReturnedIntent); 
    	switch(requestCode) {
    	case 0:
    	    if(resultCode == RESULT_OK){  
    	        Uri selectedImage = imageReturnedIntent.getData();
    	        photo.setImageURI(selectedImage);
    	        photo.setTag(selectedImage);
    	    }

    	break; 
    	case 1:
    	    if(resultCode == RESULT_OK){  
    	        Uri selectedImage = imageReturnedIntent.getData();
    	       photo.setImageURI(selectedImage);
    	       photo.setTag(selectedImage);
    	    }
    	break;
    	}
    	}
    
	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.btnGetLocation:
			
			 if(gps.canGetLocation()) {
                 
                 double latitude = gps.getLatitude();
                 double longitude = gps.getLongitude();
                
                 locationText.setText(""+latitude+","+longitude);
                   
             }
			 else {
                 // can't get location
                 // GPS or Network is not enabled
                 // Ask user to enable GPS/network in settings
                 gps.showSettingsAlert();
             }
		break;
		case R.id.btnSaveEntry:
			dataSource = new EntryDataSource(this);
	        dataSource.open();
			dataSource.createEntry(title.getText().toString(),
					               description.getText().toString(),
					               dateText.getText().toString(),
					               timeText.getText().toString(),
					               locationText.getText().toString(),
					               "");
		break;
		}
	}
}


