package com.example.locationdiary;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "locationDiary.db";
	private static final int DATABASE_VERSION = 1;


	public DatabaseHelper(Context context) {
	    super(context, DATABASE_NAME, null, DATABASE_VERSION);
	  }


	@Override
	public void onCreate(SQLiteDatabase db) {
		String sql = "CREATE TABLE entries (" +
					 "id integer primary key autoincrement," +
					 "title text," +
					 "description text," +
					 "date text," +
					 "time text," +
					 "location text," +
					 "pictureLocation text);";
					 
		
		db.execSQL(sql);		
	}


	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}


}
