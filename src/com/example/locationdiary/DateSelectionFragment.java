package com.example.locationdiary;


import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.TextView;

public class DateSelectionFragment extends DialogFragment
								   implements DatePickerDialog.OnDateSetListener {
	
	private TextView updateDate;
	
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
	    final Calendar c = Calendar.getInstance();
	    int day = c.get(Calendar.DAY_OF_MONTH);
	    int month = c.get(Calendar.MONTH);
	    int year = c.get(Calendar.YEAR);
	    

	    updateDate = (TextView) getActivity().findViewById(R.id.textViewDate);
	    
	    // Create a new instance of TimePickerDialog and return it
	    return new DatePickerDialog(getActivity(),this,year,month,day);
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		updateDate.setText(new StringBuilder().append(dayOfMonth).append("/")
				.append(monthOfYear).append("/").append(year));
		
	}
	

}
