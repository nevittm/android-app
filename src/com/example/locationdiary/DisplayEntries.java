package com.example.locationdiary;

import java.util.List;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.Menu;

public class DisplayEntries extends ListActivity {

	private EntryDataSource dataSource;
	//private DiaryEntry entry;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display_entries);
		dataSource = new EntryDataSource(this);
		dataSource.open();
		//int count = 0;
		//List<DiaryEntry> entries = dataSource.getAllEntries();
		
	    //DiaryEntry [] de = new DiaryEntry[entries.size()];
		
		/*for(@SuppressWarnings("unchecked")
		Iterator<DiaryEntry> it = entries.iterator(); it.hasNext();) {
			de[count] = it.next();
			count++;
		}
		
		setListAdapter(new EntriesArrayAdapter(this,de));
		*/
		//String time = values.get(0).getTime();
		
		// use the SimpleCursorAdapter to show the
	    // elements in a ListView
	  /*  ArrayAdapter<DiaryEntry> adapter = new ArrayAdapter<DiaryEntry>(this,
	    		android.R.layout.simple_list_item_1,values);*/
	   // setListAdapter(adapter);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.display_entries, menu);
		return true;
	}
	 
	@Override
	protected void onResume() {
	    dataSource.open();
	    super.onResume();
	}

	@Override
	protected void onPause() {
	   dataSource.close();
	   super.onPause();
	}


}
