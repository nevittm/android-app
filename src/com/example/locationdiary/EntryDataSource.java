package com.example.locationdiary;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Comment;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class EntryDataSource {
	private SQLiteDatabase database;
	private DatabaseHelper dbHelper;
	private String [] columns = { "id",
			                      "title",
			                      "description",
			                      "date",
			                      "time",
			                      "location",
			                      "pictureLocation"};
	

   public EntryDataSource(Context context) {
	   dbHelper = new DatabaseHelper(context);
   }
   
   public void open() throws SQLException {
	   database = dbHelper.getWritableDatabase();
   }
   
   public void close() {
	   dbHelper.close();
   }
   
   public List getAllEntries() {
	    List<DiaryEntry> entries = new ArrayList<DiaryEntry>();

	    Cursor cursor = database.query("entries",
	        columns, null, null, null, null, null);

	    cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	      DiaryEntry entry = cursorToEntry(cursor);
	      entries.add(entry);
	      cursor.moveToNext();
	    }
	    // make sure to close the cursor
	    cursor.close();
	    return entries;
	  }


   
   public DiaryEntry createEntry(String title, 
		                         String description, 
		                         String date, 
		                         String time,
		                         String location,
		                         String pictureLocation) {
	   
	   ContentValues values = new ContentValues();
	   				 values.put("title", title);
	   				 values.put("description", description);
	   				 values.put("date", date);
	   				 values.put("time", time);
	   				 values.put("location", location);
	   				 values.put("pictureLocation", pictureLocation);
	   long insertId = database.insert("entries", null, values);
	   
	   Cursor cursor = database.query("entries",
		        					  columns, 
		        					  "id" + " = " + 
		        					  insertId, 
		        					  null,
		        					  null,
		        					  null,
		        					  null);
		    cursor.moveToFirst();
	   DiaryEntry entry = cursorToEntry(cursor);
	   cursor.close();
   				    
	   return entry;
   }
   
   private DiaryEntry cursorToEntry(Cursor cursor) {
	    DiaryEntry entry = new DiaryEntry(cursor.getInt(0),
	    		                          cursor.getString(1),
	    		                          cursor.getString(2), 
	    		                          cursor.getString(3),
	    		                          cursor.getString(4),
	    		                          cursor.getString(5),
	    		                          cursor.getString(6));
	    
	    return entry;
	  }

}

