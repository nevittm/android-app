package com.example.locationdiary;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

public class MainActivity extends Activity implements OnClickListener {

	private ImageButton addEntryBtn;
	private ImageButton deleteEntryBtn;
	private ImageButton editEntryBtn;
	private ImageButton displayEntriesBtn;
	//private Button removeAllEntriesBtn;
	private ImageButton exitProgramBtn;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    
       addEntryBtn = (ImageButton) findViewById(R.id.btnAddEntry);
       deleteEntryBtn = (ImageButton) findViewById(R.id.btnDeleteEntry);
       editEntryBtn = (ImageButton) findViewById(R.id.btnEditEntry);
       displayEntriesBtn = (ImageButton) findViewById(R.id.btnDisplayEntries);
      // removeAllEntriesBtn = (Button) findViewById(R.id.removeAllEntriesBtn);
       exitProgramBtn = (ImageButton) findViewById(R.id.btnExitProgram);
       
       addEntryBtn.setOnClickListener(this);
       deleteEntryBtn.setOnClickListener(this);
       editEntryBtn.setOnClickListener(this);
       displayEntriesBtn.setOnClickListener(this);
       //removeAllEntriesBtn.setOnClickListener(this);
       exitProgramBtn.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


	@Override
	public void onClick(View v) {
		Intent intent = new Intent();
		
		switch(v.getId()) {
		case R.id.btnAddEntry:
			intent.setClass(this,AddEntry.class);
			
			startActivity(intent);
		break;	
		case R.id.btnDeleteEntry:
	/*		Intent deleteEntry = new Intent(this,RemoveEntry.class);
			startActivity(deleteEntry);
	*/	break;	
		case R.id.btnEditEntry:
	/*		Intent editEntry = new Intent(this,EditEntry.class);
			startActivity(editEntry);
	*/	break;	
		case R.id.btnDisplayEntries:
			Intent displayEntries = new Intent(this,DisplayEntries.class);
			startActivity(displayEntries);
		break;
		/*case R.id.removeAllEntriesBtn:
			Intent removeAllEntries = new Intent(this,RemoveEntry.class);
			startActivity(removeAllEntries);
		break;	
		*/
		case R.id.btnExitProgram:
         System.exit(0);
		break;
		}
		
	}
    
}
