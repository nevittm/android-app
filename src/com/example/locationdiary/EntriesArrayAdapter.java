package com.example.locationdiary;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class EntriesArrayAdapter extends ArrayAdapter<DiaryEntry>{
    private final Context context;
    private final DiaryEntry[] objects;
	
	
	public EntriesArrayAdapter(Context context, DiaryEntry[] objects) {
		super(context, R.layout.activity_display_entries, objects);
		this.context = context;
		this.objects = objects;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
 
		View rowView = inflater.inflate(R.layout.activity_display_entries, parent, false);
		TextView textView = (TextView) rowView.findViewById(R.id.displayTitle);
		
		if(objects[position]!=null) {
			textView.setText(objects[position].getTitle());
				
		}
		
		//Bitmap bm = BitmapFactory.decodeFile(objects[position].getPictureLocation());
        //imageView.setImageBitmap(bm);
 
		
		
		return rowView;
	
	}
}
