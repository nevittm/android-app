package com.example.locationdiary;

import java.io.Serializable;

public class DiaryEntry implements Serializable
{
  private String title;
  private String description;
  private String GPSLocation;
  private String pictureLocation;
  private String date;
  private String time;
  private int id;
  
  public DiaryEntry(int id, String title, String description, 
		            String GPSLocation, String pictureLocation, String date, String time) {
	  this.id = id;
	  this.title = title;
	  this.description = description;
	  this.GPSLocation = GPSLocation;
	  this.pictureLocation = pictureLocation;
      this.date = date;
      this.time = time;
  }

  public DiaryEntry(String title, String description, 
          String GPSLocation, String pictureLocation, String date, String time) {
	  this.title = title;
	  this.description = description;
	  this.GPSLocation = GPSLocation;
	  this.pictureLocation = pictureLocation;
	  this.date = date;
      this.time = time;
  }
  
  public void setTitle(String title) {
	  this.title = title;
  }
  
  public String getTitle() {
	  return title;
  }
  
  public void setDescription(String description) {
	  this.description = description;
  }
  
  public String getDescription() {
	  return description;
  }
  
  public int getId() {
	  return id;
  }
  
  public void setGPSLocation(String GPSLocation) {
	  this.GPSLocation = GPSLocation;
  }
  
  public String getGPSLocation() {
	  return GPSLocation;
  }
  
  public void setPictureLocation(String pictureLocation) {
	  this.pictureLocation = pictureLocation;
  }
  
  public String getPictureLocation() {
	  return pictureLocation;
  }
  
  public void setDate(String date) {
	  this.date = date;
  }
  
  public String getDate() {
	  return date;
  }
  
  public void setTime(String time) {
	  this.time = time;
  }
  
  public String getTime() {
	  return time;
  }
  
  public String toString() {
	  return title + ", " + description + ", " + date + ", " + time;
  }
}
