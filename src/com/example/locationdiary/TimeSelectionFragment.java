package com.example.locationdiary;

import java.util.Calendar;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TextView;
import android.widget.TimePicker;

public class TimeSelectionFragment extends DialogFragment
								   implements TimePickerDialog.OnTimeSetListener {
	
	private TextView updateTime;
	
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
	    final Calendar c = Calendar.getInstance();
	    int hour = c.get(Calendar.HOUR_OF_DAY);
	    int minute = c.get(Calendar.MINUTE);

	    updateTime = (TextView) getActivity().findViewById(R.id.textViewTime);
	    
	    // Create a new instance of TimePickerDialog and return it
	    return new TimePickerDialog(getActivity(), this, hour, minute,
	            DateFormat.is24HourFormat(getActivity()));	
	    }
	
	
	@Override
	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	   updateTime.setText(hourOfDay + ":" + minute);
	   
	}

}
